package checkmobile.de.hertz.helper;

import java.util.ArrayList;
import java.util.List;

import checkmobile.de.hertz.DamageActivity;
import checkmobile.de.hertz.VehicleAccessoriesActivity;
import checkmobile.de.hertz.domain.CMProcess;

/**
 * Created by icetusk on 23.05.16.
 */
public class ProcessesHelper {

    private static List<CMProcess> list=new ArrayList<>();

    static {
        list.add(new CMProcess("Vehicle accessories", VehicleAccessoriesActivity.class));
        list.add(new CMProcess("Removed accessories", VehicleAccessoriesActivity.class));
        list.add(new CMProcess("Mechanical faults", VehicleAccessoriesActivity.class));
        list.add(new CMProcess("Tyre pressure", VehicleAccessoriesActivity.class));
        list.add(new CMProcess("Capture Mileage", VehicleAccessoriesActivity.class));
        list.add(new CMProcess("Vehicle refueling", VehicleAccessoriesActivity.class));
        list.add(new CMProcess("Overview photos", VehicleAccessoriesActivity.class));
        list.add(new CMProcess("Parking", VehicleAccessoriesActivity.class));
        list.add(new CMProcess("Damage", DamageActivity.class));

        list.get(0).setFinished(true);
        list.get(0).setRequired(true);
    }

    public static List<CMProcess> getList() {
        return list;
    }

}
